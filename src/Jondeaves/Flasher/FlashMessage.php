<?php namespace Jondeaves\Flasher;


class FlashMessage
{

	private $id;
	private $type;
	private $message;
	private $dismissable;
	private $captured;
	private $expires;

	public function __construct($id, $type, $message, $dismissable, $expires)
	{
		$this->id = $id;
		$this->type = $type;
		$this->message = $message;
		$this->dismissable = $dismissable;
		$this->captured = date("Y-m-d H:i:s");
		$this->expires = $expires;
	}


	public function getMessageID()
	{
		return $this->id;
	}


	public function getType()
	{
		return $this->type;
	}


	public function getMessage()
	{
		return $this->message;
	}


	public function getDismissable()
	{
		return $this->dismissable;
	}


} 