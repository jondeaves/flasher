<?php namespace Jondeaves\Flasher\Facades;

use Illuminate\Support\Facades\Facade;

class Flasher extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'flasher'; }

} 