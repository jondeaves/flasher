<?php namespace Jondeaves\Flasher;


use Cache;
use Carbon;
use Session;
use Str;

class Flasher
{

	private $type, $message, $dismissable, $persist, $persist_time;


	public function __construct()
	{
		$this->reset();
	}


	public function reset()
	{
		if(!Session::has('flasher.messages'))
			Session::set('flasher.messages', []);

		$this->info(); // Default to info message
		$this->dismissable = true; // Default to dismissable
		$this->persist = false; // If true, message will stay longer than single session
		$this->persist_time = Carbon::now()->addHour();
	}



	/*
	 * Set severity of log. Only one active at a time
	 */
	public function success() { $this->type = 'success'; return $this; }
	public function info() { $this->type = 'info'; return $this; }
	public function warning() { $this->type = 'warning'; return $this; }
	public function danger() { $this->type = 'danger'; return $this; }


	/**
	 * Set the message to be flashed
	 *
	 * @param String $message The message to be shown for this instance
	 * @return $this Instance of self to make class chainable
	 */
	public function message($message)
	{
		$this->message = $message;
		return $this;
	}


	/**
	 * Sets if the message can be dismissed.
	 *
	 * @param bool $value Whether this message can be dismissed or not
	 * @return $this Instance of self to make class chainable
	 */
	public function dismissable($value = true)
	{
		$this->dismissable = $value;
		return $this;
	}


	/**
	 * If used, message will stay longer than single session, defaults to one hour
	 *
	 * @param integer $duration Duration in minutes
	 * @return $this
	 */
	public function persistent($duration = null)
	{
		if($duration == null)
			$duration = Carbon::now()->AddHour();

		$this->persist = true;
		$this->persist_time = $duration;
		return $this;
	}


	/*
	 * Push message to the session to be read back
	 */
	public function flash()
	{

		$message_id = Str::random(64);
		$key = 'flasher.messages.'.$message_id;
		$expires = ($this->persist) ? Carbon::now()->addMinutes($this->persist_time)->toDateTimeString() : Carbon::now()->toDateTimeString();
		$message = new FlashMessage($message_id, $this->type, $this->message, $this->dismissable, $expires);
		$message = serialize($message);

		if($this->persist)
		{
			if($this->persist_time == null)
				$this->persist_time = Carbon::now()->addHour();

			$this->add_key($message_id);
			Cache::put($key, $message, $this->persist_time);

		} // close if($this->persist)
		else
		{
			Session::flash($key, $message);
		} // close if($this->persist) [else]


		$this->reset();
	}


	/**
	 * Reverse of flash, removes a FlashMessage from storage
	 *
	 * @param $message_id Unique identifer of message to be removed
	 */
	public function forget($message_id)
	{
		$key = 'flasher.messages.'.$message_id;

		/*
		 * Remove the FlashMessage from flash session or cache
		 */
		Cache::forget($key);
		Session::forget($key);


		/*
		 * Wipe from the keys store
		 */
		$this->forget_key($message_id);
	}


	/**
	 * Read messages
	 * @return array Return array of each FlashMessage instance
	 */
	public function read()
	{

		$messages = [];


		/*
		 * Check for messages stored in flash
		 */
		$session_messages = Session::get('flasher.messages');
		foreach($session_messages as $message) {
			$message = unserialize($message);
			if($message) $messages[] = $message;
		}
		Session::forget('flasher.messages');


		/*
		 * Check for persistent messages stored in cache
		 */
		$cache_keys = Cache::get('flasher.keys');
		if(!is_array($cache_keys))
			$cache_keys = [];

		foreach($cache_keys as $key) {
			$message = unserialize(Cache::get('flasher.messages.'.$key));
			if($message) {
				$messages[] = $message;
			} else {
				Cache::forget('flasher.messages.' . $key);
			}
		}



		return $messages;

	}


	/**
	 *
	 */
	public function render()
	{
		$this->reset();
	}




	/**
	 * Add message id to permanent cache storage
	 *
	 * @param $key Unique message id
	 */
	public function add_key($key)
	{
		$cache_keys = Cache::get('flasher.keys');
		$cache_keys[] = $key;
		Cache::forever('flasher.keys', $cache_keys);
	}


	/**
	 * Remove message id from permanent cache storage
	 *
	 * @param $key Unique message id
	 */
	public function forget_key($key)
	{
		$keys = Cache::get('flasher.keys');
		$keys_new = array_diff($keys, [$key]);
		Cache::forever('flasher.keys', $keys_new);
	}

} 